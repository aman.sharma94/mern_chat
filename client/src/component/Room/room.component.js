import React, {Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';



class Room extends Component{

state = ({"roomname":"",
        "username":""
      })
handleSubmit = (e)=>{
  e.preventDefault();
console.log(e.target.elements.roomname.value);
console.log(e.target.elements.username.value);
}
render(){
  return (
    <div className="container ">
      <div className="row mt-5 d-flex justify-content-center align-self-cente ">
          <div className="col-md-6">
          <form action="" method="post" onSubmit={this.handleSubmit.bind(this)}>
          <div className="form-group">
            <label >Room Name</label>
            <input className="form-control" required type="text" name="roomname" id="roomname" />
          </div>
          <div className="form-group">
            <label >User name</label>
            <input className="form-control" type="text" required name="username" id="username"/>
          </div>
          <div className="form-group">
          <button type="submit" className="btn btn-primary">Submit</button>
          </div>
        </form>
          </div>
        
        
      </div>
    </div>
  );
  }
}

export default Room;
