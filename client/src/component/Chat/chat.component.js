import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div className="container ">
      <div className="row mt-5 d-flex justify-content-center align-self-cente ">
          <div className="col-md-6">
          <form action="" method="post">
          <div className="form-group">
            <label >Room Name</label>
            <input className="form-control" type="text" name="roomname" id="roomname"/>
          </div>
          <div className="form-group">
            <label >User name</label>
            <input className="form-control" type="text" name="username" id="username"/>
          </div>
          <div className="form-group">
          <button type="button" className="btn btn-primary">Submit</button>
          </div>
        </form>
          </div>
        
        
      </div>
    </div>
  );
}

export default App;
