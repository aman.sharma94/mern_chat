import React from 'react';
import room from './component/Room/room.component';
import chat from './component/Chat/chat.component';
import {BrowserRouter,Route} from 'react-router-dom';
function App() {
  return (
      <BrowserRouter>
      <Route exact path="/" component={room}></Route>
      <Route path="/chat" component={chat}></Route>
      </BrowserRouter>
  );
}

export default App;
